/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.externalapi.runtime.core.service;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.Map;
import org.opeantom.ubml.externalapi.runtime.api.ExternalApiEngine;
import org.openatom.ubml.externalapi.runtime.core.manager.ExternalApiInvokeManager;
import org.openatom.ubml.model.externalapi.runtime.spi.ResourceInvokerContext;

/**
 * ExternalApiEngineImpl
 *
 * @Author: Fynn Qi
 * @Date: 2019/7/22 14:03
 * @Version: V1.0
 */
public class DefaultExternalApiEngine implements ExternalApiEngine {

    /**
     * Invoke EApi service using JSON payload
     *
     * @param resourceType Type of EApi adaptation resources
     * @param resourceId resourceId
     * @param resourceOpId resourceOpId
     * @param paramList list of param(json type)
     * @return the result of invoke
     */
    @Override
    public Object invokeByJsonNode(String resourceType, String resourceId, String resourceOpId,
        List<JsonNode> paramList) {
        return ExternalApiInvokeManager
            .invokeByJsonNode(resourceType, resourceId, resourceOpId, paramList, null);
    }

    /**
     * Invoke EApi service using JSON payload
     *
     * @param resourceType Type of EApi adaptation resources
     * @param resourceId resourceId
     * @param resourceOpId resourceOpId
     * @param paramList list of param(json type)
     * @param context additional or extended context information
     * @return the result of invoke
     */
    @Override
    public Object invokeByJsonNode(String resourceType, String resourceId, String resourceOpId,
        List<JsonNode> paramList, Map<String, String> context) {
        ResourceInvokerContext resourceInvokerContext = new ResourceInvokerContext();
        resourceInvokerContext.setContextMap(context);
        return ExternalApiInvokeManager
            .invokeByJsonNode(resourceType, resourceId, resourceOpId, paramList,
                resourceInvokerContext);
    }

    /**
     * Invoke EApi service using Object payload
     *
     * @param resourceType Type of EApi adaptation resources
     * @param resourceId resourceId
     * @param resourceOpId resourceOpId
     * @param paramList list of param(object type)
     * @return the result of invoke
     */
    @Override
    public Object invokeByObject(String resourceType, String resourceId, String resourceOpId,
        List<Object> paramList) {
        return ExternalApiInvokeManager
            .invokeByObject(resourceType, resourceId, resourceOpId, paramList, null);
    }

    /**
     * Invoke EApi service using Object payload
     *
     * @param resourceType Type of EApi adaptation resources
     * @param resourceId resourceId
     * @param resourceOpId resourceOpId
     * @param paramList list of param(object type)
     * @param context additional or extended context information
     * @return the result of invoke
     */
    @Override
    public Object invokeByObject(String resourceType, String resourceId, String resourceOpId,
        List<Object> paramList, Map<String, String> context) {
        ResourceInvokerContext resourceInvokerContext = new ResourceInvokerContext();
        resourceInvokerContext.setContextMap(context);
        return ExternalApiInvokeManager
            .invokeByObject(resourceType, resourceId, resourceOpId, paramList,
                resourceInvokerContext);
    }

}
