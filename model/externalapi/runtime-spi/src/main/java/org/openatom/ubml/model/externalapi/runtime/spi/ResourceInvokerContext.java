/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.externalapi.runtime.spi;

import java.util.HashMap;
import java.util.Map;

/**
 * additional or extended context information
 *
 * @ClassName: ResourceInvokerContext
 * @Author: Fynn Qi
 * @Date: 2021/7/7 14:00
 * @Version: V1.0
 */
public class ResourceInvokerContext {

    public ResourceInvokerContext() {
        this.contextMap = new HashMap<>();
    }

    private Map<String, String> contextMap;

    public Map<String, String> getContextMap() {
        return contextMap;
    }

    public void setContextMap(Map<String, String> contextMap) {
        this.contextMap = contextMap;
    }
}
