/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.mapping;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.vo.definition.common.ViewModelMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementSourceType;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;

/**
 * The Josn Deserializer Of View Model Element Mapping
 *
 * @ClassName: GspVoElementMappingDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class GspVoElementMappingDeserializer extends ViewModelMappingDeserializer {
    @Override
    protected ViewModelMapping createVmMapping() {
        return new GspVoElementMapping();
    }

    @Override
    protected boolean readExtendMappingProperty(JsonParser reader, ViewModelMapping mapping, String propertyName) {
        boolean hasProperty = true;
        GspVoElementMapping voMapping = (GspVoElementMapping) mapping;
        switch (propertyName) {
            case ViewModelJsonConst.SourceType:
                voMapping.setSourceType(SerializerUtils
                    .readPropertyValue_Enum(reader, GspVoElementSourceType.class, GspVoElementSourceType.values(), GspVoElementSourceType.BeElement));
                break;
            case ViewModelJsonConst.TargetObjectId:
                voMapping.setTargetObjectId(SerializerUtils.readPropertyValue_String(reader));
                break;
            case ViewModelJsonConst.TargetElementId:
                voMapping.setTargetElementId(SerializerUtils.readPropertyValue_String(reader));
                break;
            default:
                hasProperty = false;
                break;
        }
        return hasProperty;
    }
}
