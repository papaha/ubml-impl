/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.action.internalexternalaction;

import org.openatom.ubml.model.vo.definition.action.MappedCdpAction;
/**
 * The Definition Of Data Mapping Action
 *
 * @ClassName: DataMappingAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class DataMappingAction extends MappedCdpAction implements IInternalExtendAction {
    public static final String ID = "42221ca3-9ee4-40af-89d2-ff4c8b466ac3";
    public static final String CODE = "DataMapping";
    public static final String NAME = "内置数据Mapping操作";

    public DataMappingAction() {
        setID(ID);
        setCode(CODE);
        setName(NAME);
    }
}
