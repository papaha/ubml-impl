/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.action;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import java.io.Serializable;
import org.openatom.ubml.model.vo.definition.action.mappedbiz.MappedBizActionParameterCollection;
import org.openatom.ubml.model.vo.definition.json.operation.MappedBizActionDeserializer;
import org.openatom.ubml.model.vo.definition.json.operation.MappedBizActionSerializer;

/**
 * The Definition Of The Mapped Biz  Action
 *
 * @ClassName: MappedBizAction
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonSerialize(using = MappedBizActionSerializer.class)
@JsonDeserialize(using = MappedBizActionDeserializer.class)
public class MappedBizAction extends ViewModelAction implements Cloneable, Serializable {

    private MappedBizActionParameterCollection mappedBizActionParams;
    /**
     * 类型
     */
    //@Override
    public ViewModelActionType Type = ViewModelActionType.BEAction;

    public MappedBizAction() {
        mappedBizActionParams = new MappedBizActionParameterCollection();
    }
    ///方法

    /**
     * 克隆
     *
     * @return VM节点映射
     */
    @Override
    public final MappedBizAction clone() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(this), MappedBizAction.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected IViewModelParameterCollection getParameters() {
        return mappedBizActionParams;
    }
}