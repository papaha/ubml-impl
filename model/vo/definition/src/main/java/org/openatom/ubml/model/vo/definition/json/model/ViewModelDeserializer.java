/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition.json.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonModel;
import org.openatom.ubml.model.common.definition.commonmodel.json.model.CommonModelDeserializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectDeserializer;
import org.openatom.ubml.model.vo.definition.GspViewModel;
import org.openatom.ubml.model.vo.definition.common.TemplateVoInfo;
import org.openatom.ubml.model.vo.definition.json.ExtendPropertiesDeserializer;
import org.openatom.ubml.model.vo.definition.json.ViewModelJsonConst;
import org.openatom.ubml.model.vo.definition.json.mapping.ViewModelMappingDeserializer;
import org.openatom.ubml.model.vo.definition.json.object.ViewObjectDeserializer;
import org.openatom.ubml.model.vo.definition.json.operation.VmActionCollectionDeserializer;

/**
 * The  Josn Deserializer Of View Model Definition
 *
 * @ClassName: BizOperationDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class ViewModelDeserializer extends CommonModelDeserializer {

  @Override
  protected GspCommonModel createCommonModel() {
    return new GspViewModel();
  }

  @Override
  protected CmObjectDeserializer createCmObjectDeserializer() {
    return new ViewObjectDeserializer();
  }

  @Override
  protected boolean readExtendModelProperty(GspCommonModel model, String propertyName,
      JsonParser jsonParser) {
    GspViewModel vm = (GspViewModel) model;
    boolean hasProperty = true;
    switch (propertyName) {
      case ViewModelJsonConst.EnableStdTimeFormat:
        vm.setEnableStdTimeFormat(SerializerUtils.readPropertyValue_boolean(jsonParser,false));
        break;
      case ViewModelJsonConst.Description:
        vm.setDescription(SerializerUtils.readPropertyValue_String(jsonParser));
        break;
      case ViewModelJsonConst.Mapping:
        readMapping(jsonParser, vm);
        break;
      case ViewModelJsonConst.ExtendType:
        SerializerUtils.readPropertyValue_String(jsonParser);
        break;
      case ViewModelJsonConst.ValueHelpConfigs:
        readValueHelpConfigs(jsonParser, vm);
        break;
      case ViewModelJsonConst.Actions:
        readVMActions(jsonParser, vm);
        break;
      case ViewModelJsonConst.ExtendProperties:
        readExtendProperties(jsonParser, vm);
        break;
      case ViewModelJsonConst.DataExtendInfo:
        readDataExtendInfo(jsonParser, vm);
        break;
      case ViewModelJsonConst.TemplateVoInfo:
        readTemplateVoInfo(jsonParser, vm);
        break;
      case ViewModelJsonConst.ConvertMsg:
        vm.setAutoConvertMessage(SerializerUtils.readPropertyValue_boolean(jsonParser));
        break;
      default:
        hasProperty = false;
        break;
    }
    return hasProperty;
  }

  private void readTemplateVoInfo(JsonParser jsonParser, GspViewModel vm) {
    JsonDeserializer<TemplateVoInfo> deserializer = new JsonDeserializer<TemplateVoInfo>() {
      @Override
      public TemplateVoInfo deserialize(JsonParser p, DeserializationContext ctxt) {
        TemplateVoInfo info = new TemplateVoInfo();
        SerializerUtils.readStartObject(jsonParser);
        while (jsonParser.getCurrentToken() == JsonToken.FIELD_NAME) {
          String propName = SerializerUtils.readPropertyName(jsonParser);
          switch (propName) {
            case ViewModelJsonConst.TemplateVoId:
              info.setTemplateVoId(SerializerUtils.readPropertyValue_String(jsonParser));
              break;
            case ViewModelJsonConst.TemplateVoPkgName:
              info.setTemplateVoPkgName(SerializerUtils.readPropertyValue_String(jsonParser));
              break;
            case ViewModelJsonConst.TemplateVoServiceUnit:
              info.setTemplateVoServiceUnit(SerializerUtils.readPropertyValue_String(jsonParser));
              break;
            default:
              throw new RuntimeException("未定义TemplateVoInfo" + propName);
          }
        }
        SerializerUtils.readEndObject(jsonParser);
        return info;
      }
    };
    try {
      vm.setTemplateVoInfo(deserializer.deserialize(jsonParser, null));
    } catch (IOException e) {
      throw new RuntimeException("'" + vm.getName() + "'" + "的TemplateVoInfo反序列化失败。");
    }
  }

  private void readValueHelpConfigs(JsonParser jsonParser, GspViewModel vm) {
    SerializerUtils
        .readArray(jsonParser, new ValueHelpConfigDeserizlizer(), vm.getValueHelpConfigs());
  }


  private void readMapping(JsonParser jsonParser, GspViewModel vm) {
    ViewModelMappingDeserializer deserializer = new ViewModelMappingDeserializer();
    vm.setMapping(deserializer.deserialize(jsonParser, null));
  }

  private void readVMActions(JsonParser jsonParser, GspViewModel vm) {
    VmActionCollectionDeserializer deserializer = new VmActionCollectionDeserializer();
    vm.setActions(deserializer.deserialize(jsonParser, null));
  }

  private void readExtendProperties(JsonParser jsonParser, GspViewModel vm) {
    ExtendPropertiesDeserializer deserializer = new ExtendPropertiesDeserializer();
    vm.setExtendProperties(deserializer.deserialize(jsonParser, null));
  }

  private void readDataExtendInfo(JsonParser jsonParser, GspViewModel vm) {
    VoDataExtendInfoDeserializer deserializer = new VoDataExtendInfoDeserializer();
    vm.setDataExtendInfo(deserializer.deserialize(jsonParser, null));
  }
}
