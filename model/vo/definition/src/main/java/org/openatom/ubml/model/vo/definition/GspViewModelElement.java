/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.vo.definition;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonElement;
import org.openatom.ubml.model.vo.definition.collection.VMActionCollection;
import org.openatom.ubml.model.vo.definition.common.VMHelpConfig;
import org.openatom.ubml.model.vo.definition.common.mapping.GspVoElementMapping;
import org.openatom.ubml.model.vo.definition.json.element.ViewElementDeserializer;

/**
 * The Definition Of View Model Element
 *
 * @ClassName: GspViewModelElement
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
@JsonDeserialize(using = ViewElementDeserializer.class)
public class GspViewModelElement extends GspCommonElement implements Serializable {
    public GspViewModelElement() {
    }

    ///属性

    /**
     * 是否启用多语输入
     * <see cref="bool"/>
     */
    private boolean enableMultiLanguageInput;

    public boolean isEnableMultiLanguageInput() {
        return enableMultiLanguageInput;
    }

    public void setEnableMultiLanguageInput(boolean enableMultiLanguageInput) {
        this.enableMultiLanguageInput = enableMultiLanguageInput;
    }

    /**
     * 是否vm新增的虚拟字段
     * <see cref="bool"/>
     */
    private boolean privateIsVirtualViewElement;

    public final boolean getIsVirtualViewElement() {
        return privateIsVirtualViewElement;
    }

    public final void setIsVirtualViewElement(boolean value) {
        privateIsVirtualViewElement = value;
    }

    /**
     是否只读属性
     <see cref="bool"/>

     //public bool IsReadOnly { get; set; }
     */

    /**
     * 是否只用于服务端
     * <see cref="bool"/>
     */
    private boolean privateIsBeckendOnly;

    public final boolean getIsBeckendOnly() {
        return privateIsBeckendOnly;
    }

    public final void setIsBeckendOnly(boolean value) {
        privateIsBeckendOnly = value;
    }

    /**
     * 隶属的VO节点
     * <see cref="GspViewObject"/>
     */
    //[Newtonsoft.Json.JsonIgnore()]
    GspViewObject getOwner() {
        return (GspViewObject)getBelongObject();

    }

    void setOwner(GspViewObject value) {
        setBelongObject(value);
    }

    /**
     * 属性映射，VMElement可以映射到BE，或者DM Element上
     * <see cref="GspVoElementMapping"/>
     */
//	private GspVoElementMapping privateMapping;
    GspVoElementMapping mapping;

    public final GspVoElementMapping getMapping() {
        return mapping;
    }

    public final void setMapping(GspVoElementMapping value) {
        mapping = value;
    }

    /**
     * 是否立即提交
     * <see cref="bool"/>
     * <p>
     * 表单上设置了立即提交控制的属性发生变更后，会自动执行Modfiy将累积变更提交到应用服务器
     */
    private boolean privateImmediateSubmission;

    public final boolean getImmediateSubmission() {
        return privateImmediateSubmission;
    }

    public final void setImmediateSubmission(boolean value) {
        privateImmediateSubmission = value;
    }

    private boolean showInFilter = true;

    /**
     * 是否在过滤中显示
     * <see cref="bool"/>
     */
    public boolean getShowInFilter() {
        return this.showInFilter;
    }

    public void setShowInFilter(boolean value) {
        this.showInFilter = value;
    }

    private boolean showInSort = true;

    /**
     * 是否在排序中显示
     * <see cref="bool"/>
     */
    public boolean getShowInSort() {
        return this.showInSort;
    }

    public void setShowInSort(boolean value) {
        this.showInSort = value;
    }

    //
    private VMHelpConfig privateVMHelpConfig;

    //	@Deprecated
    public final VMHelpConfig getVMHelpConfig() {
        return privateVMHelpConfig;
    }

    //	@Deprecated
    public final void setVMHelpConfig(VMHelpConfig value) {
        privateVMHelpConfig = value;
    }

    //
    private VMActionCollection privateHelpActions;

    //	@Deprecated
    public final VMActionCollection getHelpActions() {
        return privateHelpActions;
    }

    //	@Deprecated
    public final void setHelpActions(VMActionCollection value) {
        privateHelpActions = value;
    }

    private java.util.HashMap<String, String> extendProperties;

    /**
     * 表单拓展节点
     */
    public final java.util.HashMap<String, String> getExtendProperties() {
        if (extendProperties == null) {
            extendProperties = new java.util.HashMap<String, String>();
        }
        return extendProperties;
    }

    public void setExtendProperties(java.util.HashMap<String, String> value) {
        extendProperties = value;
    }

    ///帮助信息
    //private ElementHelpType helpType = ElementHelpType.None;
    ///// <summary>
    ///// 帮助类型
    ///// </summary>
    //public ElementHelpType HelpType
    //{
    //    get { return helpType; }
    //    set { helpType = value; }
    //}

    //private BaseDictInfo helpDefine;
    ///// <summary>
    ///// 帮助定义
    ///// </summary>
    //public BaseDictInfo HelpDefine
    //{
    //    get { return helpDefine; }
    //    set { helpDefine = value; }
    //}


    ///方法

    /**
     * 重载Equals方法
     *
     * @param obj 要比较的对象
     * @return <see cref="bool"/>
     * 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj.equals(this)) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }

        return equals((GspViewModelElement)obj);
    }

    /**
     * 当前对象是否等于同一类型的另一个对象。
     *
     * @param other 与此对象进行比较的对象。
     * @return <see cref="bool"/>
     * 如果当前对象等于 other 参数，则为 true；否则为 false。
     */
    protected boolean equals(GspViewModelElement other) {
        //&& IsReadOnly == other.IsReadOnly
        if (getID() == other.getID() && getCode() == other.getCode() && getName() == other.getName() && getObjectType() == other.getObjectType() && getIsRequire() == other.getIsRequire() && getLabelID() == other.getLabelID() && getLength() == other.getLength() && getPrecision() == other.getPrecision() && getShowInFilter() == other.getShowInFilter() && getShowInSort() == other.getShowInSort()) {
            if ((getOwner() == null && other.getOwner() == null) || (getOwner() != null && other.getOwner() != null && getOwner().getID() == other.getOwner().getID())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Serves as a hash function for a particular type.
     *
     * @return <see cref="int" />.
     * A hash code for the current
     * <filterpriority>2</filterpriority>
     */
    @Override
    public int hashCode() {
//		unchecked
        {
            //var hashCode = IsReadOnly.GetHashCode();
            int hashCode = (new Boolean(getIsBeckendOnly())).hashCode();
            hashCode = (hashCode * 397) ^ (getOwner() != null ? getOwner().hashCode() : 0);
            hashCode = (hashCode * 397) ^ (getMapping() != null ? getMapping().hashCode() : 0);
            return hashCode;
        }
    }

    /**
     * 克隆
     *
     * @return <see cref="object"/>
     */
    public final GspViewModelElement clone() {
//		Object tempVar = MemberwiseClone();
        Object tempVar = null;
        try {
            tempVar = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
//		var element = (GspViewModelElement)((tempVar instanceof GspViewModelElement) ? tempVar : null);
        GspViewModelElement element = (GspViewModelElement)((tempVar instanceof GspViewModelElement) ? tempVar : null);
        if (element == null) {
            throw new RuntimeException("克隆Element失败");
        }

        if (getMapping() != null) {
            Object tempVar2 = getMapping().clone();
            element.setMapping((GspVoElementMapping)((tempVar2 instanceof GspVoElementMapping) ? tempVar2 : null));
        }
        if (getVMHelpConfig() != null) {
            try {
                Object tempVar3 = super.clone();
                element.setVMHelpConfig((VMHelpConfig)((tempVar3 instanceof VMHelpConfig) ? tempVar3 : null));
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }

        if (getHelpActions() != null) {
            try {
                Object tempVar4 = super.clone();
                element.setHelpActions((VMActionCollection)((tempVar4 instanceof VMActionCollection) ? tempVar4 :
                        null));
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }


        return element;
    }

    /**
     * 重载ToString方法
     *
     * @return <see cref="string"/>
     */
    @Override
    public String toString() {
        return String.format("ID:%1$s,Code:%2$s,Name:%3$s", getID(), getCode(), getName());
    }
}