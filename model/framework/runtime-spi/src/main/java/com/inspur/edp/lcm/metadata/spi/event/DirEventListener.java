/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.spi.event;

import io.iec.edp.caf.commons.event.IEventListener;

public interface DirEventListener extends IEventListener {
    /**
     * 文件夹删除前事件
     *
     * @param args 文件夹事件参数
     */
    void fireDirDeletingEvent(DirEventArgs args);

    /**
     * 文件夹删除后事件
     *
     * @param args 文件夹事件参数
     */
    void fireDirDeletedEvent(DirEventArgs args);
}
