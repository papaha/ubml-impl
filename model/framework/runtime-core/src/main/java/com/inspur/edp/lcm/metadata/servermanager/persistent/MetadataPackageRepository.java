/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.servermanager.persistent;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataPackage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Classname IMetadataPackageRepository Description TODO Date 2019/8/21 16:55
 *
 * @author zhongchq
 * @version 1.0
 */
public interface MetadataPackageRepository {

    /**
     * 生成元数据包实体 从manifest中读取包信息
     *
     * @param packagePath 包的manifest文件所在的目录
     * @return
     */
    MetadataPackage getPackageEntity(String packagePath);

    /**
     * 从包中获取元数据
     *
     * @param metadataPath 元数据文件具体路径
     * @return
     */
    GspMetadata getMetadataFromPackage(String metadataPath);

    /**
     * 从指定目录下检索包文件
     *
     * @param path        指定位置（目录），一般从服务端环境信息获取包部署路径。部署路径按照应用隔离，部署路径有多个
     * @param packageName 包名称
     * @return
     */
    Map<String, MetadataPackage> retrieveSingleMetadataPackage(String path, String packageName);

    /**
     * 从指定目录下检索包文件
     *
     * @param path 指定位置（目录），一般从服务端环境信息获取包部署路径。部署路径按照应用隔离，部署路径有多个
     * @return
     */
    Map<String, MetadataPackage> retrieveAllMdPkg(String path) throws IOException;

    /**
     * 获取某一路径下的所有元数据包清单文件信息
     *
     * @param originalPath
     * @param manifestPath
     */
    void getManifestFilesPath(String originalPath, List<String> manifestPath);

    /**
     * 获取某一路径下的所有元数据包清单文件信息
     *
     * @param originalPath
     * @param mdpkgPath
     */
    void getMdpkgFilesPath(String originalPath, List<String> mdpkgPath);

    /**
     * 获取拼接路径信息
     *
     * @param path1
     * @param path2
     * @return
     */
    String getCombinePath(String path1, String path2);

    /**
     * 获取目录名
     *
     * @param path
     * @return
     */
    String getDirectoryName(String path);

    /**
     * 根据元数据包文件，获取元数据包信息，读取manifest文件
     *
     * @param packageFile
     * @return MetadataPackage
     */
    MetadataPackage getMetadataPackage(File packageFile) throws IOException;

}
