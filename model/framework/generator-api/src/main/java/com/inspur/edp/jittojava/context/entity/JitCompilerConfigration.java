/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.context.entity;

/**
 * @Classname JitCompilerConfigration
 * @Description TODO
 * @Date 2019/7/29 16:13
 * @Created by zhongchq
 * @Version 1.0
 */
public class JitCompilerConfigration {

    private String typeCode;
    private Boolean enable;
    private CompilerConfigData beforeCompiler;
    private CompilerConfigData afterCompiler;

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public CompilerConfigData getBeforeCompiler() {
        return beforeCompiler;
    }

    public void setBeforeCompiler(CompilerConfigData beforeCompiler) {
        this.beforeCompiler = beforeCompiler;
    }

    public CompilerConfigData getAfterCompiler() {
        return afterCompiler;
    }

    public void setAfterCompiler(CompilerConfigData afterCompiler) {
        this.afterCompiler = afterCompiler;
    }

}
