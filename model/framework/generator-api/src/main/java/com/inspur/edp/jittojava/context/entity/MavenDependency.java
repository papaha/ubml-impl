/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.jittojava.context.entity;

/**
 * @Classname MavenDependency
 * @Description TODO
 * @Date 2019/12/16 15:04
 * @Created by liu_bintr
 * @Version 1.0
 */
public class MavenDependency {

    private String artifactId;
    private String version;
    private String file;
    private String groupId;

    public MavenDependency(String groupId, String artifactId, String version) {
        setGroupId(groupId);
        setArtifactId(artifactId);
        setVersion(version);
    }

    public MavenDependency(String groupId, String artifactId, String version, String file) {
        setGroupId(groupId);
        setArtifactId(artifactId);
        setVersion(version);
        setFile(file);
    }

    public MavenDependency() {

    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MavenDependency)) {
            return false;
        }
        MavenDependency md = (MavenDependency) obj;
        return (groupId == null ? md.getGroupId() == null : groupId.equals(md.getGroupId())) &&
            (artifactId == null ? md.getArtifactId() == null : artifactId.equals(md.getArtifactId()));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (groupId == null ? 0 : groupId.hashCode());
        result = 31 * result + (artifactId == null ? 0 : artifactId.hashCode());
        return result;
    }
}
