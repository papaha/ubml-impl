/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.index;

import com.inspur.edp.lcm.metadata.api.entity.MetadataPackageForIndex;
import com.inspur.edp.lcm.metadata.common.Utils;
import java.util.HashMap;
import java.util.Map;

public class MetadataPackageIndexServiceForMaven extends MetadataPackageIndexService {

    private static MetadataPackageIndexServiceForMaven instance;
    private final Map<String, MetadataPackageForIndex> metadataPackageIndexForMavenHashMap;

    public MetadataPackageIndexServiceForMaven(String location) {
        this.location = location;
        depFileSuffix = Utils.getPomSuffix();
        metadataPackageIndexHashMap = metadataPackageIndexForMavenHashMap = new HashMap<>();
    }

    public static MetadataPackageIndexServiceForMaven getInstance(String location) {
        if (instance == null || (location != null && !location.equals(instance.location))) {
            instance = new MetadataPackageIndexServiceForMaven(location);
        }
        return instance;
    }

    public Map<String, MetadataPackageForIndex> getMetadataPackageIndexForMavenHashMap() {
        if (metadataPackageIndexForMavenHashMap == null || metadataPackageIndexForMavenHashMap.size() == 0) {
            refreshMetadataPackageIndex();
        }
        return metadataPackageIndexForMavenHashMap;
    }
}
