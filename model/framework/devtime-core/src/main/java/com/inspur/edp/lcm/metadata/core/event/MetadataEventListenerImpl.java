/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.event;

import com.inspur.edp.lcm.metadata.api.entity.GspMetadata;
import com.inspur.edp.lcm.metadata.api.entity.MetadataProject;
import com.inspur.edp.lcm.metadata.core.MetadataCoreManager;
import com.inspur.edp.lcm.metadata.core.MetadataProjectCoreService;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventArgs;
import com.inspur.edp.lcm.metadata.spi.event.MetadataEventListener;
import java.util.List;

public class MetadataEventListenerImpl implements MetadataEventListener {
    MetadataCoreManager manager = new MetadataCoreManager();

    @Override
    public void fireMetadataSavingEvent(MetadataEventArgs args) {

    }

    @Override
    public void fireMetadataSavedEvent(MetadataEventArgs args) {
        MetadataCoreManager.getCurrent().removeMetadataListCache(args.getPath());
    }

    @Override
    public void fireMetadataDeletingEvent(MetadataEventArgs args) {
        // 找到工程下的所有元数据
        MetadataProject project = MetadataProjectCoreService.getCurrent().getMetadataProjInfo(args.getPath());
        List<GspMetadata> metadataList = MetadataCoreManager.getCurrent().getMetadataList(project.getProjectPath());

        metadataList.forEach(metadata -> {
            if (metadata.getRefs() != null) {
                metadata.getRefs().forEach(item -> {
                    if (item.getDependentMetadata().getId().equals(args.getMetadata().getHeader().getId())) {
                        throw new RuntimeException("此元数据被" + metadata.getHeader().getFileName() + "依赖，请清除其中依赖后重试。");
                    }
                });
            }
        });
    }

    @Override
    public void fireMetadataDeletedEvent(MetadataEventArgs args) {
        manager.removeMetadataListCache(args.getPath());
    }
}
