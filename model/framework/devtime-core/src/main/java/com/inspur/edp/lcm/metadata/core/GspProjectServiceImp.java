/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core;

import com.inspur.edp.lcm.metadata.api.ConfigData.gspproject.ProjectConfiguration;
import com.inspur.edp.lcm.metadata.api.entity.GspProject;
import com.inspur.edp.lcm.metadata.api.service.GspProjectService;
import com.inspur.edp.lcm.metadata.devcommon.ManagerUtils;
import java.util.List;

/**
 * GSP工程服务
 *
 * @author zhaoleitr
 */
public class GspProjectServiceImp implements GspProjectService {

    private GspProjectCoreService gspProjectCoreService = new GspProjectCoreService();

    /**
     * 获取GSP工程的基本信息
     *
     * @param path 工程路径
     * @return Gsp工程
     */
    @Override
    public GspProject getGspProjectInfo(String path) {
        if (path.isEmpty()) {
            throw new RuntimeException("路径不能为空");
        }
        String absolutePath = ManagerUtils.getAbsolutePath(path);
        return gspProjectCoreService.getGspProjectInfo(absolutePath);
    }

    @Override
    public List<ProjectConfiguration> getGspProjectTypeInfo() {
        return gspProjectCoreService.getGspProjectTypeInfo();
    }
}
