/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.core.event;

import io.iec.edp.caf.commons.event.CAFEventArgs;
import io.iec.edp.caf.commons.event.EventBroker;
import io.iec.edp.caf.commons.event.IEventListener;
import io.iec.edp.caf.commons.event.config.EventListenerData;
import io.iec.edp.caf.commons.event.config.EventListenerSettings;

public class MetadataEventBroker extends EventBroker {
    private MetadataEventManager metadataEventManager;

    public MetadataEventBroker(EventListenerSettings settings) {
        super(settings);
        this.metadataEventManager = new MetadataEventManager();
        this.init();
    }

    @Override
    protected void onInit() {
        this.eventManagerCollection.add(metadataEventManager);
    }

    public void fireMetadataSavingEvent(CAFEventArgs args) {
        this.metadataEventManager.fireMetadataSavingEvent(args);
    }

    public void fireMetadataSavedEvent(CAFEventArgs args) {
        this.metadataEventManager.fireMetadataSavedEvent(args);
    }

    public void fireMetadataDeletingEvent(CAFEventArgs args) {
        this.metadataEventManager.fireMetadataDeletingEvent(args);
    }

    public void fireMetadataDeletedEvent(CAFEventArgs args) {
        this.metadataEventManager.fireMetadataDeletedEvent(args);
    }

    /**
     * 测试异常是否被抛出
     */
    public IEventListener createEventListenerTest(EventListenerData eventListenerData) {
        return this.createEventListener(eventListenerData);
    }

    /**
     * 测试注销事件
     *
     * @param eventListener
     */
    public void removeListenerTest(IEventListener eventListener) {
        this.removeListener(eventListener);
    }
}
