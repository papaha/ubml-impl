/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.extend.action;

import com.inspur.edp.lcm.metadata.api.entity.DeploymentContext;
import com.inspur.edp.lcm.metadata.api.service.FileService;
import com.inspur.edp.lcm.project.deployer.spi.DeployAction;
import io.iec.edp.caf.commons.utils.SpringBeanUtils;
import java.io.File;
import java.nio.file.Paths;

public class DeployActionImpl implements DeployAction {
    @Override
    public void deploy(DeploymentContext context) {
        File publishFile = new File(context.getPublishPath());
        FileService fileService = SpringBeanUtils.getBean(FileService.class);
        File[] files = publishFile.listFiles();
        for (File file : files) {
            if (file.getName().equals("jstack")) {
                String serverRtPath = Paths.get(context.getDebugEnvironment().getServerPath(), "server").toString();
                if (fileService.isDirectoryExist(serverRtPath)) {
                    fileService.folderCopy(file.getPath(), serverRtPath);
                } else {
                    fileService.folderCopy(file.getPath(), Paths.get(context.getDebugEnvironment().getServerPath(), file.getName()).toString());
                }
            } else {
                fileService.folderCopy(file.getPath(), Paths.get(context.getDebugEnvironment().getServerPath(), file.getName()).toString());
            }
        }
    }
}
