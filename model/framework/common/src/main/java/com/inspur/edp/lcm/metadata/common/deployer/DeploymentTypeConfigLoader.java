/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.deployer;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inspur.edp.lcm.metadata.api.entity.deployer.DeploymentType;
import com.inspur.edp.lcm.metadata.common.FileServiceImp;
import com.inspur.edp.lcm.metadata.common.Utils;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class DeploymentTypeConfigLoader {

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    private String serverPath;
    protected String fileName = "tools/deploy/project_java/config/platform/common/lcm_deploymenttype.json";
    private String sectionName = "DeploymentType";

    private List<DeploymentType> deploymentTypeConfiguration;

    public DeploymentTypeConfigLoader() {
        serverPath = Paths.get(Utils.getStartupPath()).resolve("../..").toString().replace("\\", "/");
    }

    protected List<DeploymentType> loadDeploymentTypeConfiguration() {
        if (deploymentTypeConfiguration != null) {
            return deploymentTypeConfiguration;
        }
        try {
            fileName = Paths.get(serverPath).resolve(fileName).toString();
            deploymentTypeConfiguration = getDeploymentTypeConfiguration(fileName, sectionName);
            return deploymentTypeConfiguration;
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            fileName = "tools/deploy/project_java/config/platform/common/lcm_deploymenttype.json";
        }
        return null;
    }

    protected DeploymentType getDeploymentType(String typeCode) {
        List<DeploymentType> deploymentTypes = loadDeploymentTypeConfiguration();
        DeploymentType deploymentType = deploymentTypes.stream().filter(type -> type.getTypeCode().equals(typeCode)).findFirst().orElse(null);
        return deploymentType;
    }

    private List<DeploymentType> getDeploymentTypeConfiguration(String path, String sectionName) throws IOException {
        FileServiceImp fileService = new FileServiceImp();
        String fileContents = fileService.fileRead(path);
        ObjectMapper objectMapper = Utils.getMapper();
        JsonNode jsonNode = objectMapper.readTree(fileContents);
        String deploymentType = jsonNode.findValue(sectionName).toString();
        return objectMapper.readValue(deploymentType, new TypeReference<List<DeploymentType>>() {
        });
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }
}
