/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.entity;

public class DebugEnvironment {
    private String serverPath;
    private String language;
    private String deployPath;
    private String deployConfigPath;
    private DbConnectionInfo dbConnectionInfo;
    private DbConnectionInfo centralDbConnectionInfo;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getServerPath() {
        return serverPath;
    }

    public void setServerPath(String serverPath) {
        this.serverPath = serverPath;
    }

    public String getDeployPath() {
        return deployPath;
    }

    public void setDeployPath(String deployPath) {
        this.deployPath = deployPath;
    }

    public String getDeployConfigPath() {
        return deployConfigPath;
    }

    public void setDeployConfigPath(String deployConfigPath) {
        this.deployConfigPath = deployConfigPath;
    }

    public DbConnectionInfo getDbConnectionInfo() {
        return dbConnectionInfo;
    }

    public void setDbConnectionInfo(DbConnectionInfo dbConnectionInfo) {
        this.dbConnectionInfo = dbConnectionInfo;
    }

    public DbConnectionInfo getCentralDbConnectionInfo() {
        return centralDbConnectionInfo;
    }

    public void setCentralDbConnectionInfo(DbConnectionInfo centralDbConnectionInfo) {
        this.centralDbConnectionInfo = centralDbConnectionInfo;
    }
}
