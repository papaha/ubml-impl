/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.compiler;

import com.inspur.edp.lcm.metadata.api.entity.compiler.MetadataCompilerConfiguration;
import com.inspur.edp.lcm.metadata.common.configuration.MetadataServiceHelper;
import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class MetadataCompilerConfigLoader {
    private String fileName = "config/platform/common/lcm_gspprojectextend.json";
    private String sectionName = "MetadataCompilerConfigration";
    private List<MetadataCompilerConfiguration> compileConfigrations;

    protected List<MetadataCompilerConfiguration> loadCompileConfigration() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            if (compileConfigrations == null || compileConfigrations.size() <= 0) {
                fileName = Paths.get(EnvironmentUtil.getServerRTPath()).resolve(fileName).toString();
                compileConfigrations = metadataServiceHelper.getCompileConfigrationList(fileName, sectionName);
                return compileConfigrations;
            } else {
                return compileConfigrations;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected MetadataCompilerConfiguration getCompileConfigurationData(String typeName) {
        loadCompileConfigration();
        if (compileConfigrations != null && compileConfigrations.size() > 0) {
            for (MetadataCompilerConfiguration data : compileConfigrations) {
                if (data.getTypeCode().equalsIgnoreCase(typeName)) {
                    return data;
                }
            }
        } else {
            return null;
        }
        return null;
    }
}
