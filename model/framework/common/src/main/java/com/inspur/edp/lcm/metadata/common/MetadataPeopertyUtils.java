/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common;

public class MetadataPeopertyUtils {
    public static String header = "Header";
    public static String content = "Content";
    public static String extendRule = "ExtendRule";
    public static String refs = "Refs";
    public static String extendProperty = "ExtendProperty";
    public static String properties = "Properties";
    public static String version = "Version";
    public static String previousVersion = "PreviousVersion";
    public static String relativePath = "RelativePath";
    public static String extended = "Extended";
}
