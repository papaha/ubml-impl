/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.common.extractor;

import com.inspur.edp.lcm.metadata.api.entity.extract.ExtractConfigration;
import com.inspur.edp.lcm.metadata.common.configuration.MetadataServiceHelper;
import io.iec.edp.caf.common.environment.EnvironmentUtil;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

public class ExtractConfigLoader {
    private String fileName = "config/platform/common/lcm_gspprojectextend.json";
    private String sectionName = "ExtractConfigration";
    private List<ExtractConfigration> extractConfigrations;

    /**
     * @param
     * @return java.util.List<com.inspur.gsp.lcm.metadata.entity.MetadataConfiguration>
     * @throws
     * @author zhongchq
     * @description 获取配置文件数据
     * @date 15:40 2019/7/23
     **/
    protected List<ExtractConfigration> loadExtractConfigration() {
        MetadataServiceHelper metadataServiceHelper = new MetadataServiceHelper();
        try {
            if (extractConfigrations == null || extractConfigrations.size() <= 0) {
                fileName = Paths.get(EnvironmentUtil.getServerRTPath()).resolve(fileName).toString();
                extractConfigrations = metadataServiceHelper.getExtractConfigrationList(fileName, sectionName);
                return extractConfigrations;
            } else {
                return extractConfigrations;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    protected ExtractConfigration getExtractConfigurationData(String typeName) {
        loadExtractConfigration();
        if (extractConfigrations != null && extractConfigrations.size() > 0) {
            for (ExtractConfigration data : extractConfigrations) {
                if (data.getTypeCode().equalsIgnoreCase(typeName)) {
                    return data;
                }
            }
        } else {
            return null;
        }
        return null;
    }
}
