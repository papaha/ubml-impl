/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.inspur.edp.lcm.metadata.api.ConfigData.gspproject;

/**
 * 工程配置信息 用于读取配置文件中的工程配置
 */
public class ProjectConfiguration {

    /**
     * 工程类型基础配置信息 {@link ProjectCommonConfigData}
     */
    private ProjectCommonConfigData commonConfigData;

    /**
     * 工程类型创建扩展配置信息 {@link ProjectCreateConfigData}
     */
    private ProjectCreateConfigData createConfigData;

    /**
     * 工程类型中包含的开发对象类别
     */
    private String includeDevObjTypes;

    public ProjectCommonConfigData getCommonConfigData() {
        return commonConfigData;
    }

    public void setCommonConfigData(ProjectCommonConfigData commonConfigData) {
        this.commonConfigData = commonConfigData;
    }

    public ProjectCreateConfigData getCreateConfigData() {
        return createConfigData;
    }

    public void setCreateConfigData(ProjectCreateConfigData createConfigData) {
        this.createConfigData = createConfigData;
    }

    public String getIncludeDevObjTypes() {
        return includeDevObjTypes;
    }

    public void setIncludeDevObjTypes(String includeDevObjTypes) {
        this.includeDevObjTypes = includeDevObjTypes;
    }
}
