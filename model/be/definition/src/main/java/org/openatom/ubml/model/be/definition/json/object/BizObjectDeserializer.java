/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.object;

import com.fasterxml.jackson.core.JsonParser;
import java.io.IOException;
import org.openatom.ubml.model.be.definition.GspBizEntityObject;
import org.openatom.ubml.model.be.definition.LogicDeleteControlInfo;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.be.definition.json.element.BizElementDeserializer;
import org.openatom.ubml.model.be.definition.json.operation.BizActionCollectionDeserializer;
import org.openatom.ubml.model.be.definition.json.operation.DtmCollectionDeserializer;
import org.openatom.ubml.model.be.definition.json.operation.ValCollectionDeserializer;
import org.openatom.ubml.model.be.definition.operation.collection.BizActionCollection;
import org.openatom.ubml.model.be.definition.operation.internalbeaction.DeleteBEAction;
import org.openatom.ubml.model.be.definition.operation.internalbeaction.ModifyBEAction;
import org.openatom.ubml.model.be.definition.operation.internalbeaction.RetrieveBEAction;
import org.openatom.ubml.model.common.definition.cef.collection.CommonValCollection;
import org.openatom.ubml.model.common.definition.cef.entity.GspCommonDataType;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.cef.json.operation.CommonValDeserializer;
import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonObject;
import org.openatom.ubml.model.common.definition.commonmodel.entity.object.GspCommonObjectType;
import org.openatom.ubml.model.common.definition.commonmodel.json.element.CmElementDeserializer;
import org.openatom.ubml.model.common.definition.commonmodel.json.object.CmObjectDeserializer;

/**
 * The  Josn Deserializer Of Entity
 *
 * @ClassName: BizObjectDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizObjectDeserializer extends CmObjectDeserializer {
    @Override
    protected CmObjectDeserializer createCmObjectDeserializer() {
        return new BizObjectDeserializer();
    }

    @Override
    protected boolean ReadExtendObjectProperty(GspCommonObject commonObject, String propName, JsonParser jsonParser) {
        boolean result = true;
        GspBizEntityObject object = (GspBizEntityObject)commonObject;
        switch (propName) {
            case BizEntityJsonConst.DETERMINATIONS:
                readDeterminations(jsonParser, object);
                break;
            case BizEntityJsonConst.VALIDATIONS:
                readValidations(jsonParser, object);
                break;
            case BizEntityJsonConst.BIZ_ACTIONS:
                readBizActions(jsonParser, object);
                break;
            case BizEntityJsonConst.AUTH_FIELD_INFOS:
                readAuthFieldInfos(jsonParser, object);
                break;
            case BizEntityJsonConst.VALIDATION_AFTER_SAVE:
                CommonValCollection collection = new CommonValCollection();
                CommonValDeserializer der = new CommonValDeserializer();
                SerializerUtils.readArray(jsonParser, der, collection);
                break;
            case BizEntityJsonConst.LOGIC_DELETE_CONTROL_INFO:
                object.setLogicDeleteControlInfo(SerializerUtils.readPropertyValue_Object(LogicDeleteControlInfo.class, jsonParser));
                try {
                    jsonParser.nextToken();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                result = false;
        }
        return result;
    }

    private void readAuthFieldInfos(JsonParser jsonParser, GspBizEntityObject object) {
        SerializerUtils.readStringArray(jsonParser);
    }

    private void readBizActions(JsonParser jsonParser, GspBizEntityObject object) {
        BizActionCollectionDeserializer deserializer = new BizActionCollectionDeserializer();
        object.setBizActions(deserializer.deserialize(jsonParser, null));
    }

    private void readValidations(JsonParser jsonParser, GspBizEntityObject object) {
        ValCollectionDeserializer deserializer = new ValCollectionDeserializer();
        object.setValidations(deserializer.deserialize(jsonParser, null));
    }

    private void readDeterminations(JsonParser jsonParser, GspBizEntityObject object) {
        DtmCollectionDeserializer deserializer = new DtmCollectionDeserializer();
        object.setDeterminations(deserializer.deserialize(jsonParser, null));
    }

    @Override
    protected GspCommonObject CreateCommonObject() {
        return new GspBizEntityObject();
    }

    @Override
    protected CmElementDeserializer CreateElementDeserializer() {
        return new BizElementDeserializer();
    }

    @Override
    protected void afterReadCommonDataType(GspCommonDataType commonDataType) {
        GspBizEntityObject bizObject = (GspBizEntityObject)commonDataType;
        if (bizObject.getObjectType() == GspCommonObjectType.MainObject) {
            BizActionCollection actions;
            actions = null;
            if (bizObject.getBizActions() != null && bizObject.getBizActions().size() > 0) {
                actions = bizObject.getBizActions().clone();
                bizObject.getBizActions().clear();
            }

            bizObject.getBizActions().add(new RetrieveBEAction());
            bizObject.getBizActions().add(new ModifyBEAction());
            bizObject.getBizActions().add(new DeleteBEAction());

            if (actions != null) {
                bizObject.getBizActions().addAll(actions);
            }
        }
    }
}
