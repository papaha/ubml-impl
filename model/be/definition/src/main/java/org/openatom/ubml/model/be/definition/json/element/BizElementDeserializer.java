/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.element;

import com.fasterxml.jackson.core.JsonParser;
import org.openatom.ubml.model.be.definition.GspBizEntityElement;
import org.openatom.ubml.model.be.definition.beenum.RequiredCheckOccasion;
import org.openatom.ubml.model.be.definition.common.BizEntityJsonConst;
import org.openatom.ubml.model.common.definition.cef.json.SerializerUtils;
import org.openatom.ubml.model.common.definition.commonmodel.entity.GspCommonElement;
import org.openatom.ubml.model.common.definition.commonmodel.json.element.CmElementDeserializer;

/**
 * The  Josn Deserializer Of Be Element
 *
 * @ClassName: BizElementDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizElementDeserializer extends CmElementDeserializer {
    @Override
    protected final GspBizEntityElement createElement() {
        return new GspBizEntityElement();
    }

    @Override
    protected boolean readExtendElementProperty(GspCommonElement item, String propName, JsonParser jsonParser) {
        boolean result = true;
        GspBizEntityElement field = (GspBizEntityElement)item;
        switch (propName) {
            case BizEntityJsonConst.CALCULATION_EXPRESS:
                field.setCalculationExpress(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.VALIDATION_EXPRESS:
                field.setValidationExpress(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            case BizEntityJsonConst.REQUIRED_CHECK_OCCASION:
                field.setRequiredCheckOccasion(SerializerUtils.readPropertyValue_Enum(jsonParser, RequiredCheckOccasion.class, RequiredCheckOccasion.values()));
                break;
            case BizEntityJsonConst.IS_DEFAULT_NULL:
                field.setIsDefaultNull(SerializerUtils.readPropertyValue_boolean(jsonParser));
                break;
            case BizEntityJsonConst.UNIFIED_DATA_TYPE:
                //todo:json-文件中无Udt信息,为null
                SerializerUtils.readPropertyValue_String(jsonParser);
                break;
            case BizEntityJsonConst.ELEMENT_CONFIG_ID:
                field.setRtElementConfigId(SerializerUtils.readPropertyValue_String(jsonParser));
                break;
            default:
                result = false;
                break;
        }
        return result;
    }
}
