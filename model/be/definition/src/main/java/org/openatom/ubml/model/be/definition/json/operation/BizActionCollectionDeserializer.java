/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.json.operation;

import org.openatom.ubml.model.be.definition.common.InternalActionUtil;
import org.openatom.ubml.model.be.definition.operation.BizOperation;
import org.openatom.ubml.model.be.definition.operation.collection.BizActionCollection;

/**
 * The  Josn Deserializer Of Entity Action Colleciton
 *
 * @ClassName: BizActionCollectionDeserializer
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BizActionCollectionDeserializer extends BizOperationCollectionDeserializer<BizActionCollection> {
    @Override
    protected BizActionCollection createCollection() {
        return new BizActionCollection();
    }

    @Override
    protected BizActionDeserializer createDeserializer() {
        return new BizActionDeserializer();
    }

    @Override
    protected void addBizOp(BizActionCollection collection, BizOperation op) {
        if (!InternalActionUtil.InternalBeActionIDs.contains(op.getID())) {
            super.addBizOp(collection, op);
        }
    }
}
