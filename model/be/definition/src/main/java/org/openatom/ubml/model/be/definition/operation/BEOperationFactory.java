/*
 *  Copyright © OpenAtom Foundation.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.openatom.ubml.model.be.definition.operation;

import org.openatom.ubml.model.be.definition.beenum.BEOperationType;

/**
 * The Factory Of Biz Operation,Using It,You Can Create Operation Or Something Else
 *
 * @ClassName: BEOperationFactory
 * @Author: Benjamin Gong
 * @Date: 2021/1/11 17:13
 * @Version: V1.0
 */
public class BEOperationFactory {
    /**
     * 根据操作类型创建BOBizOperation
     *
     * @param operationType 操作类型
     * @return
     */
    public static BizOperation createElement(BEOperationType operationType) {
        BizOperation result;
        switch (operationType) {
            case BizAction:
            case Subscription:
            case Initiation:
                result = new BizAction();
                break;
            case BizMgrAction:
                result = new BizMgrAction();
                break;
            case Validation:
                result = new Validation();
                break;
            case Determination:
                result = new Determination();
                break;
            default:
                result = null;
                break;
        }
        return result;
    }
}